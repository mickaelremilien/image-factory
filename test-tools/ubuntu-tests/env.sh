IMAGE="$URCHIN_IMG_ID"
TESTENV="./.current-run.env.sh"
FLAVOR_STD="n1.cw.standard-1"
FLAVOR_ALT="n1.cw.standard-2"
NETWORK="3644adbb-c38d-4208-bd62-3be90c006c51"
FLOATING_IP_POOL="9c70b2ca-e31a-4597-9dfd-95722acd36c7"
KEYPAIR="jenkins-ci"
PRIVATE_KEY="/var/lib/jenkins/.ssh/jenkins-ci.pem"
SSH_USER="cloud"
HOST="google.com"
LOG_FILE="/tmp/test-ubuntu.log"
USER_DATA_FILE="./userdata.yml"

if [ -f "$TESTENV" ]; then
    . $TESTENV
fi
